package com.example.demo.services;
import java.util.List;

import com.example.demo.entitites.User;
import com.example.demo.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    UserRepository userRepository;

    private static final Logger l = LogManager.getLogger(UserServiceImpl.class);

    @Override
    public List<User> retrieveAllUsers() {
        List<User> users = null;
        try {

            l.info("try to get users");
            users = (List<User>) userRepository.findAll();
            for (User user : users) {
                l.info(user.toString());
            }
            l.info("these are all your users");
        }catch (Exception e) {
            l.error("there are no users");
        }

        return users;
    }


    @Override
    public User addUser(User u) {
        l.info("adding user");
        User u_saved = userRepository.save(u);
        l.info(" user added successfully");
        return u_saved;
    }

    @Override
    public User updateUser(User u) {
        l.info("updating user");
        User u_saved = userRepository.save(u);
        l.info(" user updated successfully");
        return u_saved;
    }

    @Override
    public void deleteUser(Long id) {
        l.info(" deleting user");
        userRepository.deleteById(id);
        l.info(" user deleted successfully");
    }

    @Override
    public User retrieveUser(Long id) {
        l.info(" getting user ");
        //User u =  userRepository.findById(Long.parseLong(id)).orElse(null);
        User u =  userRepository.findById(id).get();
        l.info(" user getted successfully");
        return u;
    }

}