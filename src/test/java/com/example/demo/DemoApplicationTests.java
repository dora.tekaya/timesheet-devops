package com.example.demo;

import com.example.demo.entitites.Role;
import com.example.demo.entitites.User;
import com.example.demo.services.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import static org.junit.Assert.*;

@SpringBootTest
class DemoApplicationTests {

    @Autowired
    private UserServiceImpl us;


    @Test
    void retrieveAllUsers() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2015-03-23");
        us.addUser(new User("dorra","tkaya",date, Role.CHEF_DEPARTEMENT));
        us.addUser(new User("adnen","tkaya",date, Role.ADMINISTRATEUR));
        assertTrue(us.retrieveAllUsers().size()>0);
    }

    @Test
    void addUser() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2015-03-23");
        User user1 = us.addUser(new User("ala","tkaya",date, Role.CHEF_DEPARTEMENT));
        assertNotNull(user1);
    }

    @Test
    void updateUser() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2015-03-23");
        User u1 = new User("sabeh","tkaya",date, Role.CHEF_DEPARTEMENT);
        us.addUser(u1);
        u1.setRole(Role.INGENIEUR);
        User u2 = us.updateUser(u1);
        assertEquals(Role.INGENIEUR,u2.getRole());
    }

    @Test
    void deleteUser() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2015-03-23");
        User u1 = new User("mahdi","tkaya",date, Role.CHEF_DEPARTEMENT);
        us.addUser(u1);
        assertNotNull(u1);
        int before = us.retrieveAllUsers().size();
        us.deleteUser(u1.getId());
        int after = us.retrieveAllUsers().size();
        assertEquals(after+1,before);
    }

    @Test
    void retrieveUser() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2015-03-23");
        User u1 = new User("rached","tkaya",date, Role.CHEF_DEPARTEMENT);
        us.addUser(u1);
        assertNotNull(u1);
        us.retrieveUser(u1.getId());
        assertTrue(u1.getLastName().equals("tkaya"));
    }


}
